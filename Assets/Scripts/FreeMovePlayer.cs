using UnityEngine;
using System.Collections;

public class FreeMovePlayer : MonoBehaviour {

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("enemy"))
            BattleController.InitiateCombat();
    }
}
