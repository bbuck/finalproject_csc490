using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum HealthChangeType {
	Heal,
	Damage
}

public class HealthEvent {
	public int CurrentHealth { get; set; }
	public int MaxHealth { get; set; }
	public GameObject GameObject { get; set; }
	public HealthChangeType ChangeType { get; set; }
}

public class HealthSystem : MonoBehaviour {
	
	#region events
	
	public delegate void HealthChangedDelegate(HealthEvent evt);
	public event HealthChangedDelegate HealthChanged = delegate {};
	
	#endregion events
	
	#region properties
	
	public int maxHealth = 0;
	
	private int adjustedMax = 0;
	private int currentHealth = 0;

    public int CurrentHealth
    {
        get
        {
            return currentHealth;
        }
    }

	#endregion properties
	
	#region unity functions
	
	void Start() {
		currentHealth = adjustedMax = maxHealth;	
	}
	
	#endregion unity functions
	
	#region public functions
	
	public void AdjustMaxHealth(int amount) {
		adjustedMax = maxHealth + amount;
	}
	
	public void ResetMaxHealth() {
		adjustedMax = maxHealth;	
	}
	
	public void SetMaxHealth(int max) {
		int diff = adjustedMax - maxHealth;
		maxHealth = max;
		adjustedMax = maxHealth + diff;
	}
	
	public void Damage(int amount) {
		AdjustCurrentHealth(-amount);
		FireHealthChanged(HealthChangeType.Damage);
	}
	
	public void Heal(int amount) {
		AdjustCurrentHealth(amount);
		FireHealthChanged(HealthChangeType.Heal);
	}
	
	public void ChangeHealth(int amount) {
		if (amount < 0)
			Damage(-amount); // Takes positive amount
		else 
			Heal(amount);
	}
	
	#endregion public functions
	
	#region helper functions
	
	void AdjustCurrentHealth(int amount) {
		currentHealth += amount;
		currentHealth = Mathf.Clamp(currentHealth, 0, adjustedMax);
	}
	
	void FireHealthChanged(HealthChangeType type) {
		HealthEvent evt = new HealthEvent();
		evt.CurrentHealth = currentHealth;
		evt.MaxHealth = adjustedMax;
		evt.GameObject = gameObject;
		evt.ChangeType = type;
		HealthChanged(evt);
	}
	
	#endregion helper functions
}
