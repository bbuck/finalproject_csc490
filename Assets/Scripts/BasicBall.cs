using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]
public class BasicBall : MonoBehaviour {
	
	#region properties
	
	const int MinDamageMult = 1;
	const int MaxDamageMult = 3;
	
	public float minSpeed = 5f;
	public float maxSpeed = 15f;
    public int damage = 1;
	public float damageMultiplierIncr = 0.2f;
	
	
	private float damageMult = MinDamageMult;
	private float colorIncrement = 0.03921f; // Temp
	private float currentSpeed;
	private Vector2 direction = Vector2.zero;
	
	#endregion properties
	
	#region unity Functions
	
	void Start() 
    {
		currentSpeed = minSpeed;
		direction.x = 0.3f;
		direction.y = 0.5f;
        rigidbody.velocity = new Vector3(direction.x, 0, direction.y) * currentSpeed;
	}

	void Update() 
    {
        if (rigidbody.velocity.magnitude < currentSpeed)
        {
            rigidbody.velocity = rigidbody.velocity.normalized * currentSpeed;
        }
        transform.rotation = Quaternion.LookRotation(rigidbody.velocity.normalized);
	}

    void OnCollisionEnter(Collision collision)
    {
        CollisionHandler(collision);
    }

	#endregion unity Functions
	
	#region helper Functions
	
	protected virtual void CollisionHandler(Collision collision) {
        GameObject obj = collision.gameObject;
		if (obj.CompareTag("brick")) {
			int dealt = Mathf.RoundToInt(damage * damageMult);
			obj.GetComponent<BasicBrick>().Hit(dealt);
			damageMult = Mathf.Clamp(damageMult + damageMultiplierIncr, MinDamageMult, MaxDamageMult);
		}
        else if (obj.CompareTag("back_wall"))
			damageMult = MinDamageMult;
	}
	
	#endregion helper Functions
}
