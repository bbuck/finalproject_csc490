using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(HealthSystem))]
public class BasicBrick : MonoBehaviour {
	
	#region properties
	
	public HealthSystem Health { get; private set; }
	
	#endregion properties
	
	#region unity functions
	
	void Start() {
		OnStart();
	}
	
	#endregion unity functions
	
	#region public functions
	
	public void Hit(int damage) {
		Health.Damage(damage);	
	}
	
	#endregion public functions
	
	#region helper functions
	
	protected virtual void OnStart() {
		Health = GetComponent<HealthSystem>();
		Health.HealthChanged += OnHealthChanged;
	}
	
	protected virtual void OnHealthChanged(HealthEvent evt) {
		if (evt.CurrentHealth == 0) {
			Destroy(gameObject);	
		}
	}
	
	#endregion helper functions
}
