Shader "Custom/FlowingColoredNoise" {
  Properties {
    _Color ("Main Color (RGBA)", Color) = (1, 1, 1, 1)
	_FlowColor ("Flow Color (RGB)", Color) = (1, 1, 1, 1)
    _Noise ("Noise", 2D) = "white"{}
	_Speed ("Speed", Range(1, 10)) = 1
  }
  SubShader {
    Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
    LOD 200
    
    CGPROGRAM
    #pragma surface surf Lambert alpha

    sampler2D _Noise;
    half4 _Color;
	half4 _FlowColor;
	float _Speed;

    struct Input {
      half2 uv_Noise;
    };

    void surf (Input IN, inout SurfaceOutput o) {
      float time = _Time[0] * _Speed;
      half2 uvs = IN.uv_Noise + time;
      half3 noise = tex2D(_Noise, uvs).rgb;
	  half3 flow = noise + _FlowColor;
      half3 color = _Color.rgb * flow;

      o.Albedo = color;
      o.Alpha = _Color.a;
    }
    ENDCG
  } 
  FallBack "Diffuse"
}
