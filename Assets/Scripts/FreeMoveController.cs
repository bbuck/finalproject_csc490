using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(CharacterMotor))]
public class FreeMoveController : MonoBehaviour 
{
	
	#region Properties
	
	public float speed = 3.0f;
	
	private CharacterMotor Motor { get; set; }
	
	#endregion Properties
	
	#region Unity Functions
	
	void Start() 
    {
		Motor = GetComponent<CharacterMotor>();
	}
	
	void Update() 
    {		
		Motor.targetVelocity.x = Input.GetAxis("Horizontal") * speed;
		Motor.targetVelocity.z = Input.GetAxis("Vertical") * speed;
		
		print(Motor.targetVelocity);
	}
	
	#endregion Unity Functions
}
