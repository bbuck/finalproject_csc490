using UnityEngine;
using System.Collections;

public class LightFlicker : MonoBehaviour
{

    #region Properties

    public Light flickeringLight;
    public float minFlickerSpeed = 0.2f;
    public float maxFlickerSpeed = 0.6f;
    public float minRangeDiff = 0.5f;
    public float maxRangeDiff = 0.5f;

    private float baseRange, minRange, maxRange;

    #endregion Properties

    #region Unity functions

    void Start()
    {
        if (flickeringLight) 
        {
            baseRange = flickeringLight.range;
            StartCoroutine("Flicker");
        }
    }

    #endregion Unity functions

    #region helper functions

    IEnumerator Flicker()
    {
        while (true)
        {
            float rangeDiff = Random.Range(minRangeDiff, maxRangeDiff);
            flickeringLight.range = baseRange + rangeDiff;

            float nextFlicker = Random.Range(minFlickerSpeed, maxFlickerSpeed);
            yield return new WaitForSeconds(nextFlicker);
        }
    }

    #endregion helper functions
}
