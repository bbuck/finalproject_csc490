using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(HealthSystem))]
public class HealthSystemEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        HealthSystem health = (HealthSystem)target;

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Current Health", health.CurrentHealth.ToString());
        EditorGUILayout.EndHorizontal();
    }
}
